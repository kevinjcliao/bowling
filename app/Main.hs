module Main where

import Lib

main :: IO ()
main = do 
    putStrLn "Please enter the bowling score."
    score <- getLine
    case game score of
        Just result -> putStrLn $ "The score of your game is: " ++ show result
        Nothing     -> putStrLn "The score is invalid."
