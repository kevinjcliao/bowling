module Lib where


import GHC.Base
import GHC.Char
import GHC.Real (fromIntegral)
import GHC.Show
import GHC.Read (readLitChar, lexLitChar)
import GHC.Unicode
import GHC.Num

data LastState = Regular | Spare | Strike1 | Strike2 deriving (Eq, Show)

-- The default implementation of digitToInt throws an error. 
-- I'm modifying it to use the maybe monad. 
readDigit :: Char -> Maybe Int
readDigit c
  | c == '-'                       = Just 0
  | (fromIntegral dec::Word) <= 9  = Just dec
  | otherwise = Nothing
  where
    dec = ord c - ord '0'

removeWhiteSpace :: String -> String
removeWhiteSpace = filter (/= ' ')

readScore :: String -> Maybe (Int, LastState, String)
readScore ('X' : rest)   = Just (10, Strike1, rest)
readScore (a : b : rest) = case readDigit a of
    Just num -> if b=='/' 
        then
            Just (10, Spare, rest)
        else
            case readDigit b of
                Just num2 -> Just (num + num2, Regular, rest)
                Nothing   -> Nothing
    Nothing  -> Nothing
readScore _                    = Nothing

bowl :: String -> [LastState] -> Maybe Int
bowl []    _                       = Just 0
bowl score (Regular : laterStates) = case readScore score of
    Just (nextScore, state, rest) -> case bowl rest (state : laterStates) of
        Just val -> Just $ val + nextScore
        Nothing  -> Nothing
    Nothing -> Nothing
bowl score (Spare : laterStates ) = case readScore score of
    Just (nextScore, state, rest) -> case bowl rest (state : laterStates) of
        Just val -> Just $ val + 2 * nextScore
        Nothing  -> Nothing
    Nothing -> Nothing
bowl score (Strike1 : laterStates ) = case readScore score of
    Just (nextScore, state, rest) -> 
        case bowl rest (Strike2 : (state : laterStates)) of
            Just val -> Just $ val + 2 * nextScore
            Nothing  -> Nothing
    Nothing -> Nothing
bowl score (Strike2 : laterStates ) = case readScore score of
    Just (nextScore, state, rest) -> 
        case bowl rest (state : laterStates) of
            Just val -> Just $ val + 2 * nextScore
            Nothing  -> Nothing
    Nothing -> Nothing

game :: String -> Maybe Int
game score = bowl (removeWhiteSpace score) [Regular]
