import Test.Tasty
import Test.Tasty.HUnit

import Lib

main :: IO ()
main = defaultMain (testGroup "all tests." [testReadScore, testGames])

testReadScore :: TestTree
testReadScore = testGroup 
    "readScoreTests" 
    [ testStrike
    , testSpare
    , testReg
    , testBadStringToRead
    , testBadChar
    , testBadSpareToRead
    , testBowl
    ]

testStrike :: TestTree
testStrike = testCase "Testing a Strike."
    (assertEqual "Strike should work." (readScore "X ") (Just (10, Strike1, " ")))

testSpare :: TestTree
testSpare = testCase "Testing a Spare."
    (assertEqual "Spare should work." (readScore "5/ ") (Just (10, Spare, " ")))

testReg :: TestTree
testReg = testCase "Testing a Regular Score."
    (assertEqual "Regular should work." (readScore "53 ") (Just (8, Regular, " ")))

testBadStringToRead :: TestTree
testBadStringToRead = testCase "Testing a Bad String."
    (assertEqual "Bah! You're reading a bad string." Nothing (readScore "aeosnuhaoenstuh"))

testBadChar :: TestTree
testBadChar = testCase "Testing a Bad Character."
    (assertEqual "Bah! You're reading a bad character!" Nothing (readScore "a "))

testBadSpareToRead :: TestTree
testBadSpareToRead = testCase "Testing a Bad Spare."
    (assertEqual "Bah! You're reading a bad spare." Nothing (readScore "/5 "))

testGames :: TestTree
testGames = testGroup
    "game tests."
    [ testRegularGame
    ]

testBowl :: TestTree
testBowl = testCase "Testing a Strike."
    (assertEqual "Bah! Wrong score for a strike." (Just 16) (bowl "X 1 2" [Regular]))

testRegularGame :: TestTree
testRegularGame = testCase "Testing a regular game with no special scores."
    (assertEqual "Bah! A regular game doesn't work." (Just 90) (game "9- 9- 9- 9- 9- 9- 9- 9- 9- 9- "))
    
    